import sys, os
import sqlite3
import json
import spacy
import argparse
import pandas as pd

def annotate(data, lang, annotation, nlp):
    content = data.get(lang+"_text")
    doc = nlp(content)
    token_ids = dict([(t.idx, t.i) for t in doc])
    ner = dict([(t.i, 'O') for t in doc]) # type: dict[int | None , str ]
    for a in annotation:
        if a.get('from_name').startswith(lang) and a.get('value').get("text"):
            start = a.get('value').get('start')
            end = a.get('value').get('end')
            text = a.get('value').get('text')
            label = a.get('value').get('labels')[0]
            # find the closest start from left
            while content[start]==' ':
                start += 1
                text = text.strip()
#            print("annotation", a)
#            print("content[start:end]", content[start:end], "text", text)
            if len([t for t in doc if t.idx==start])==0: # no token starts at this position
                # find the closest token beginning to the left
                while start>0 and len([t for t in doc if t.idx==start])==0:
                    start = start-1
                    text = doc.text[start:end]
            if len([t for t in doc if t.idx==start])==0:
                print("NO TOKEN starts at position", start, "!")
                print("Sentence", content, "NER", content[start:end], "should be", text, "/", label)
            if start in token_ids.keys():
                token_id = token_ids.get(start)
                ner[token_id]='B-'+label
                # this fixes small annotation errors such as marked trailing punctuation, parentheses
                # @ at the beginning of persons ID (e.g., X nickname)
                # most problematic is a trailing comma (e.g., in initial), it is part of the entity, in contrast,
                # an entity at the end of sentence should be without the comma.
                for i in range(token_id+1, len(doc)):
                    if doc[i].idx+len(doc[i].text)<=end:
                        if len(doc[i].text)==1 and doc[i].idx+1==end:
                            if doc[i].text in ',;?-([:v“”_!‘':
#                                print("SKIP the last punctuation at", text)
                                continue
                            elif doc[i].text in ')' and len([t for t in text if t==')'])>len([t for t in text if t=='(']):
#                                print("SKIP the last ) parenthesis at", text)
                                continue
                            elif doc[i].text in ']' and len([t for t in text if t==']'])>len([t for t in text if t=='[']):
#                                print("SKIP the last ] parenthesis at", text)
                                continue
                            elif doc[i].text=='.' and len([t for t in text if t=='.'])==1:
                                if 'CC ' not in text: # license versions
#                                    print("SKIP the last comma at", text)
                                    continue
                        ner[i]='I-'+label
    for t in doc:
        if t.i>0 and t.text=="'s" and ner[t.i]!='O':
#            print("Annotate", t.text, "as", doc[t.i-1], ner[t.i-1], doc.text)
            ner[t.i] = ner[t.i-1].replace('B-', 'I-')
    return [(doc[i].text, doc[i].idx, ner_type) for i, ner_type in ner.items()]


def read_database(db_file, project_id):
    # Check if the database file exists
    if not os.path.exists(db_file):
        print(f"Error: Database file '{db_file}' does not exist.")
        sys.exit(1)

    # Connecting to sqlite
    # connection object

    conn = sqlite3.connect(db_file)

    # cursor object
    cursor = conn.cursor()

    # Check if required tables exist in the database
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table' AND name IN ('task', 'task_completion');")
    tables = set(row[0] for row in cursor.fetchall())
    if 'task' not in tables or 'task_completion' not in tables:
        print("Error: The database does not contain the required tables 'task' or 'task_completion'.")
        sys.exit(1)

    # this select can be modified to extract prediction.result instead of task_completion.result if needed
    statement = '''
    select task_completion.task_id, 
           task.data, 
           task_completion.result 
    from task_completion, task 
    where task.project_id=? and task.id=task_completion.task_id;
    '''

    cursor.execute(statement, (project_id,))

    annotations = {}
    output = cursor.fetchall()
    for row in output:
        annotations[row[0]] = (json.loads(row[1]), json.loads(row[2]))

    conn.commit()

    # Close the connection
    conn.close()
    return annotations

def tokenize_annotations(annotations,nlp, lang):
    data = {}
    for annotation in annotations.values():
        text, true_labels = annotation
        filename = text.get('document')+'.iob'
        if filename not in data.keys():
            data[filename] = []
        token_labels = []

        for token, start, label in annotate(text, lang, true_labels, nlp):
            if token.strip():
                token_labels.append((token, start, label))

        data[filename].append(
            (text.get('index'), text.get('document'), text.get('score'), text.get('alignment_type'), token_labels))
    return data

def write_files(data, output_dirname):
    for filename in data.keys():
        sentences = sorted(data[filename], key=lambda x: x[0])
        if sentences:
            with open(os.path.join(output_dirname, filename), mode='w', encoding="utf-8") as f:
                for i, _, _, _, s in sentences:
                    for token, _, label in s:
                        f.write('{}\t{}\n'.format(token, label))

def create_dataframe(data):
    res = []

    for filename in data.keys():
        document_offset = 0
        sentences = sorted(data[filename], key=lambda x: x[0])
        for i, document, score, alignment_type, s in sentences:
            #        print("sentence", s)
            if s:
                for token, start, label in s:
                    res.append({"document": document,
                                "index": i,
                                "score": score,
                                "alignment": alignment_type,
                                "token": token,
                                "start": start + document_offset,
                                "label": label})
                #            print("s", s[-1], s[-1][1], len(s[-1][0]))
                document_offset = document_offset + s[-1][1] + len(s[-1][0]) + 1

    return pd.DataFrame(res)

def main():
    parser = argparse.ArgumentParser(
        description="Extract annotated data from LabelStudio database and save in either IOB of CSV formats."
    )
    parser.add_argument("language_code", help="3-letter language code for processing (e.g., 'eng')")
    parser.add_argument("database_file", help="Path to the LabelStudio SQLite database file")
    parser.add_argument("project_id", help="LabelStudio project ID")
    parser.add_argument("output_dir", help="Output directory for saving results")
    parser.add_argument(
        "format",
        choices=["iob", "csv"],
        nargs="?",
        default="iob",
        help="Output format: IOB (default) or CSV"
    )

    args = parser.parse_args()

    lang = args.language_code
    database_file = args.database_file
    project_id = args.project_id
    output_dirname = args.output_dir
    output_format = args.format

    if not os.path.exists(output_dirname):
        os.mkdir(output_dirname)

    print("Reading", lang, "ground truth from db", database_file, ", and saving to", output_dirname, "as", output_format)
    nlp = spacy.blank("en")
    nlp.add_pipe("token_splitter")
    # Spacy tokenization can differ slightly from what is expected, e.g., entities with dashes can be split

    annotations = read_database(database_file, project_id)
    print("len annotations", len(annotations.keys()))

    data = tokenize_annotations(annotations, nlp, lang)
    print("len data", len(data.keys()))

    if output_format=="csv":
        df = create_dataframe(data)
        filename = f"pgv_{lang}_ner.csv"
        with open(os.path.join(output_dirname, filename), 'w', encoding='utf-8') as f:
            df.to_csv(f, sep='\t')
    else:
        write_files(data, output_dirname)


if __name__ == "__main__":
    main()
