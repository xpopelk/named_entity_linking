# named_entity_linking
This is an initial project. The up-to-date version moved to https://gitlab.fi.muni.cz/nlp/named_entity_linking.

## Name
Choose a self-explaining name for your project.

## Description
This projects adds new annotation layers over (some) data from the [Parallel Global Voices corpus](https://nlp.ilsp.gr/pgv/) (PGV).
We start with the cze-eng language pair, annotate named entities of four classes (PER, ORG, LOC, MISC), and link named entities to WikiData whenever possible.

The project contains:
- annotation templates for [LabelStudio](https://labelstud.io/),
- annotation manual,
- scripts that extract annotated data from the LabelStudio database and convert them in the commonly used formats,
- annotated data.


<!--## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.
-->

## Installation
If you want to replicate the process, please do the following:
- install LabelStudio
- select a NER model for pre-annotation
- optionally select a model for spacyopentapioca

## Usage
### Prepare and convert the data
- download PGV data (in HTML since we extract sentence numbers) in [data/get_data.sh](data/get_data.sh)
- extract aligned sentences using [01_prepare_annotation/read_html_corpus.py](01_prepare_annotation/read_html_corpus.py)\
`python read_html_corpus.py ../data/pgv/orig/ ../data/`
- extract complete documents using [01_prepare_annotation/read_corpus.py](01_prepare_annotation/read_corpus.py)\
`python read_corpus.py ../data/corpus.csv ../data/txt/`

### Prepare for manual annotation/check
- pre-annotate using your favourite NER model [02_ner_annotation/bert-ner.py](02_ner_annotation/bert-ner.py)\
`python bert-ner.py ../data/txt/<lang>/ <your_model> data/ner/<lang>`.\
For example, for English, use the dslim models:\
`python bert-ner.py ../data/txt/eng/ dslim/bert-base-NER  ../data/ner/eng`
- insert preannotations into LabelStudio database [02_ner_annotation/convert_preannotated_document.py](02_ner_annotation/convert_preannotated_document.py)\
`python convert_preannotated_document.py ../data/corpus.csv ../data/ner/ ../data/ "cesCzert+engBert"`
- set up LabelStudio:
  - create new project
  - insert generated json file\
    ![insert generated json file](labelstudio_uploaddata.png)
  - select custom template and insert it from [labelstudio/template.xml](labelstudio/template.xml) (feel free to adjust the field names)\
    ![insert annotation template](labelstudio_template.png)
  - annotate

### Prepare NEL annotation

The NEL annotation is made per file since processing using opentapioca takes some time.

- extract annotations from the LabelStudio database [03_prepare_nel_annotation/extract_document.py](03_prepare_nel_annotation/extract_document.py)\
`python extract_document.py <document-name> <path_to_label_studio.sqlite3> <project_id> ../data/ner_json/`
- use spacyopentapioca to pre-annotate links to WikiData [03_prepare_nel_annotation/nel_annotation.py](03_prepare_nel_annotation/nel_annotation.py)\
`python nel_annotation.py <ner_annotated_json> ../data/nel`
- upload json files to LabelStudio\
    ![NEL annotation](labelstudio_nel.png)
- annotate

The `project_id` is the number in the LabelStudio database. You can find it e.g. in the URL to annotation tasks.

Known issue: An entity can be ambiguous, e.g., a person's name can be linked to multiple WikiData QNames. 
Mostly, in such cases, only one sense is used in the text. However, there are exceptions such as "Facebook" that can be linked to the platform name or the former company name in the same text. In this case, the annotation has to be adjusted later.

### Perform cross-lingual NEL
TODO

### Export NER data in IOB format

The annotated entities can be exported into the IOB format using [04_export/extract_iob_fromdb.py](04_export/extract_iob_fromdb.py)\
`python extract_iob_fromdb.py eng <path_to_label_studio.sqlite3> <project_id> ../data/export/iob/`

The `project_id` is the number in the LabelStudio database. You can find it e.g. in the URL to annotation tasks.

### Export NER data to CSV

Annotated NEs can be exported into CSV using [04_export/export_to_csv.py](04_export/export_to_csv.py)\
`python export_to_csv_ner.py eng <path_to_label_studio.sqlite3> <project_id> ../data/export/csv/pgv_eng_ner.csv`

### Export NEL data to CSV

This script takes the NER export in CSV and adds a new column "nel": [04_export/export_nel_annotation.py](04_export/export_nel_annotation.py)\
`python export_nel_annotation.py <path_to_label_studio.sqlite3> <project_id> ../data/export/csv/pgv_eng_ner.csv ../data/export/nel.csv`

Note that the `project_id` is the number of the NEL annotation project. It will be a different number than NER annotation project ids.

<!--
## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.
-->

## Contributing
If you want to contribute with another language pair, that's great. Please contact us.

## Authors and acknowledgment
The original PGV paper:

[Parallel Global Voices: a Collection of Multilingual Corpora with Citizen Media Stories](https://aclanthology.org/L16-1144) (Prokopidis et al., LREC 2016)

TSD paper:

TODO

## License

https://nlp.fi.muni.cz/en/LicenceWebService
