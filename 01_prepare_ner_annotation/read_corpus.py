import sys
import os
import pandas as pd

if len(sys.argv)<3:
   print("Usage: read_corpus.py <corpus_filename.csv> [output_dir]")
   sys.exit(0)

corpus_filename = sys.argv[1]
output_dirname = sys.argv[2]

print("Processing", corpus_filename, "to directory", output_dirname)

df = pd.read_csv(corpus_filename, sep='\t', keep_default_na=False)

languages = [x for x in df.columns if x.startswith('text')]
lang1 = languages[0].split('_')[-1]
lang2 = languages[1].split('_')[-1]
print("lang1", lang1, "lang2", lang2)

if not os.path.exists(os.path.join(output_dirname, lang1)):
   os.mkdir(os.path.join(output_dirname, lang1))
if not os.path.exists(os.path.join(output_dirname, lang2)):
   os.mkdir(os.path.join(output_dirname, lang2))

for i, group in df.groupby(by='filename'):
    filename = group.filename.iloc[0]
    print("filename", filename)
    text1 = group['text_'+lang1].values
    text2 = group['text_'+lang2].values
    with open(os.path.join(output_dirname, lang1+'/'+filename+'.txt'), 'w') as f:
        f.write('\n'.join(text1))
    with open(os.path.join(output_dirname, lang2+'/'+filename+'.txt'), 'w') as f:
        f.write('\n'.join(text2))
